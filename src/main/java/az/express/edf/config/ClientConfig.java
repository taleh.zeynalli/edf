package az.express.edf.config;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.net.URI;

@Component
public class ClientConfig {

    @Value("${edf.key.prod}")
    private String edfKey;

    @SneakyThrows
    public <T> RequestEntity<T> requestEntity(String url, HttpMethod method, T body) {
        MultiValueMap<String, String> headers=new HttpHeaders();
        headers.add("key", edfKey);
        return new RequestEntity<T>(body, headers, method, new URI(url));
    }

    public String documentPath() {
        String pfxPath = "";
        String osname = System.getProperty( "os.name" );
        if (osname.contains( "Windows" )) {
            pfxPath = "C:\\Users\\e.allahverdiyev\\Desktop\\document";

        } else if (osname.contains( "Linux")) {
            pfxPath = "/opt/edf/document/document-";
        }
        return pfxPath;
    }
}