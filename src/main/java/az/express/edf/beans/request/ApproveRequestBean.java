package az.express.edf.beans.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class ApproveRequestBean {

    private BigDecimal amount;
    private Integer duration;
    private BigDecimal interestRate;
    private BigDecimal commission;
    private Integer gracePeriod;

}
