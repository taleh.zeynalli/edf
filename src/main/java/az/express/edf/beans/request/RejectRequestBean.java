package az.express.edf.beans.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RejectRequestBean {

    private String rejectReason;
}
