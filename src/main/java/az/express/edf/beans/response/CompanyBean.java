package az.express.edf.beans.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyBean {
            private Long id;
            private String companyName;
            private String registrationForm;
            private String serialNumber;
            private String pin;
            private LocalDate incorporationDate;
            private String legalAddress;
            private String actualAddress;

}
