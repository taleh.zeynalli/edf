package az.express.edf.beans.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class StakeholdersBean {

    private Long id;
    private String tin;
    private String fullName;
    private String contactNumber;
    private String email;

}
