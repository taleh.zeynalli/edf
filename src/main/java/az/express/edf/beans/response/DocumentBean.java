package az.express.edf.beans.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class DocumentBean {
    private Long id;
    private String fileName;
}
