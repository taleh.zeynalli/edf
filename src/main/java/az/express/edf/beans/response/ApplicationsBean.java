package az.express.edf.beans.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicationsBean {
    private Long id;
    private BigDecimal amount;
    private Integer duration;
    private String purpose;
    private String loanType;
    private String status;

    private BigDecimal interestRate;
    private BigDecimal commission;
    private LocalDate offerDate;
    private BigDecimal offerAmount;
    private Integer offerDuration;
    private Integer offerGracePeriod;
    private String taxesStatus;
    private String bankStatus;
    private String edfStatus;

    private String bankRejectReason;
    private String taxesRejectReason;
    private String edfRejectReason;

    private LocalDate submittedDate;
    private Integer gracePeriod;

    private CompanyBean company;

    private List<StakeholdersBean> stakeholders;

    private DocumentBean document;
}
