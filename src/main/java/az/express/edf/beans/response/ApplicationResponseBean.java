package az.express.edf.beans.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
public class ApplicationResponseBean {

    List<ApplicationsBean> applications;
    private Boolean hasNextPage;
    private Long lastPageNumber;


}
