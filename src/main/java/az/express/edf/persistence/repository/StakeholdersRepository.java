package az.express.edf.persistence.repository;

import az.express.edf.persistence.entity.Stakeholders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StakeholdersRepository extends JpaRepository<Stakeholders, Long> {

    Stakeholders findByStakeholdersId(Long id);
}
