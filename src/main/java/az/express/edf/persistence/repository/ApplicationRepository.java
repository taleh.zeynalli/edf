package az.express.edf.persistence.repository;

import az.express.edf.persistence.entity.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application,Long> {

    Application findByApplicationId(Long id);
}
