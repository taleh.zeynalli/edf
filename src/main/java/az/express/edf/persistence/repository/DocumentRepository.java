package az.express.edf.persistence.repository;

import az.express.edf.persistence.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document,Long> {

    Document findByDocumentId(Long id);
}
