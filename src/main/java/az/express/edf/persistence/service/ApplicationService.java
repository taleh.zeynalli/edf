package az.express.edf.persistence.service;

import az.express.edf.beans.response.ApplicationsBean;

public interface ApplicationService {

    void save(ApplicationsBean responseBean);

    void update(ApplicationsBean responseBean);

    void update(Long id);
}
