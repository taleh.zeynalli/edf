package az.express.edf.persistence.service.impl;

import az.express.edf.beans.response.*;
import az.express.edf.enums.EDFSTATUSENUM;
import az.express.edf.persistence.entity.Application;
import az.express.edf.persistence.entity.Company;
import az.express.edf.persistence.entity.Document;
import az.express.edf.persistence.entity.Stakeholders;
import az.express.edf.persistence.repository.ApplicationRepository;
import az.express.edf.persistence.service.ApplicationService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {

    private final static Logger LOGGER = LogManager.getLogger(ApplicationServiceImpl.class);

    private final ApplicationRepository applicationRepository;


    @Override
    public void save(ApplicationsBean applicationsBean) {
        try {

            Application application = applicationRepository.findByApplicationId(applicationsBean.getId());

            if (Objects.isNull(application)) {
                application = new Application();
                application.setApplicationId(applicationsBean.getId());
                application.setAmount(applicationsBean.getAmount());
                application.setDuration(applicationsBean.getDuration());
                application.setPurpose(applicationsBean.getPurpose());
                application.setLoanType(applicationsBean.getLoanType());
                application.setStatus(applicationsBean.getStatus());
                application.setInterestRate(applicationsBean.getInterestRate());
                application.setCommission(applicationsBean.getCommission());
                application.setOfferDate(applicationsBean.getOfferDate());
                application.setOfferAmount(applicationsBean.getOfferAmount());
                application.setOfferDuration(applicationsBean.getOfferDuration());
                application.setOfferGracePeriod(applicationsBean.getOfferGracePeriod());
                application.setTaxesStatus(applicationsBean.getTaxesStatus());
                application.setBankStatus(applicationsBean.getBankStatus());
                application.setEdfStatus(applicationsBean.getEdfStatus());
                application.setBankRejectReason(applicationsBean.getBankRejectReason());
                application.setTaxesRejectReason(applicationsBean.getTaxesRejectReason());
                application.setEdfRejectReason(applicationsBean.getEdfRejectReason());
                application.setSubmittedDate(applicationsBean.getSubmittedDate());
                application.setGracePeriod(applicationsBean.getGracePeriod());


                if (Objects.nonNull(applicationsBean.getCompany())) {
                    CompanyBean companyBean = applicationsBean.getCompany();
                    Company company = new Company();
                    company.setCompanyId(companyBean.getId());
                    company.setCompanyName(companyBean.getCompanyName());
                    company.setRegistrationForm(companyBean.getRegistrationForm());
                    company.setSerialNumber(companyBean.getSerialNumber());
                    company.setPin(companyBean.getPin());
                    company.setIncorporationDate(companyBean.getIncorporationDate());
                    company.setLegalAddress(companyBean.getLegalAddress());
                    company.setActualAddress(companyBean.getActualAddress());
                    application.setCompany(company);
                }

                List<Stakeholders> stakeholdersList = new ArrayList<>();
                for (StakeholdersBean s : applicationsBean.getStakeholders()) {
                    Stakeholders stakeholders = new Stakeholders();
                    stakeholders.setStakeholdersId(s.getId());
                    stakeholders.setTin(s.getTin());
                    stakeholders.setFullName(s.getFullName());
                    stakeholders.setContactNumber(s.getContactNumber());
                    stakeholders.setEmail(s.getEmail());
                    stakeholdersList.add(stakeholders);
                }
                application.setStakeholders(stakeholdersList);

                if (Objects.nonNull(applicationsBean.getDocument())) {
                    DocumentBean documentBean = applicationsBean.getDocument();
                    Document document = new Document();
                    document.setDocumentId(documentBean.getId());
                    document.setFileName(documentBean.getFileName());
                    application.setDocument(document);
                }

                applicationRepository.save(application);
                LOGGER.info("Save DB Applications! ID - " + applicationsBean.getId());
            }

        } catch (Exception e) {
            LOGGER.error("Non save DB Applications!");
            e.printStackTrace();
        }
    }

    @Override
    public void update(ApplicationsBean applicationsBean) {
        try {
            Application application = applicationRepository.findByApplicationId(applicationsBean.getId());
            if (Objects.nonNull(application)) {
                application.setApplicationId(applicationsBean.getId());
                application.setAmount(applicationsBean.getAmount());
                application.setDuration(applicationsBean.getDuration());
                application.setPurpose(applicationsBean.getPurpose());
                application.setLoanType(applicationsBean.getLoanType());
                application.setStatus(applicationsBean.getStatus());
                application.setInterestRate(applicationsBean.getInterestRate());
                application.setCommission(applicationsBean.getCommission());
                application.setOfferDate(applicationsBean.getOfferDate());
                application.setOfferAmount(applicationsBean.getOfferAmount());
                application.setOfferDuration(applicationsBean.getOfferDuration());
                application.setOfferGracePeriod(applicationsBean.getOfferGracePeriod());
                application.setTaxesStatus(applicationsBean.getTaxesStatus());
                application.setBankStatus(applicationsBean.getBankStatus());
                application.setEdfStatus(applicationsBean.getEdfStatus());
                application.setBankRejectReason(applicationsBean.getBankRejectReason());
                application.setTaxesRejectReason(applicationsBean.getTaxesRejectReason());
                application.setEdfRejectReason(applicationsBean.getEdfRejectReason());
                application.setSubmittedDate(applicationsBean.getSubmittedDate());
                application.setGracePeriod(applicationsBean.getGracePeriod());


                CompanyBean companyBean = applicationsBean.getCompany();
                if (Objects.nonNull(companyBean)) {
                    application.getCompany().setCompanyId(companyBean.getId());
                    application.getCompany().setCompanyName(companyBean.getCompanyName());
                    application.getCompany().setRegistrationForm(companyBean.getRegistrationForm());
                    application.getCompany().setSerialNumber(companyBean.getSerialNumber());
                    application.getCompany().setPin(companyBean.getPin());
                    application.getCompany().setIncorporationDate(companyBean.getIncorporationDate());
                    application.getCompany().setLegalAddress(companyBean.getLegalAddress());
                    application.getCompany().setActualAddress(companyBean.getActualAddress());
                }
                DocumentBean documentBean = applicationsBean.getDocument();
                if (Objects.nonNull(documentBean)) {
                    application.getDocument().setDocumentId(documentBean.getId());
                    application.getDocument().setFileName(documentBean.getFileName());
                }
                applicationRepository.save(application);
                LOGGER.info("Update  DB Applications! ID - " + applicationsBean.getId());
            }

        } catch (Exception e) {
            LOGGER.error("Non Update DB Applications!");
            e.printStackTrace();
        }
    }

    @Override
    public void update(Long id) {
        Application application = applicationRepository.findByApplicationId(id);
        application.setDocumentReviewDate(new Date());
        application.setBankStatus(EDFSTATUSENUM.WAITING_FOR_BANK_REVIEW.name());
        applicationRepository.save(application);
    }
}
