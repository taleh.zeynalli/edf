package az.express.edf.persistence.service;


public interface DocumentService {
    void update(Long id, String filePath);
}
