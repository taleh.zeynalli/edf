package az.express.edf.persistence.service.impl;

import az.express.edf.beans.response.DocumentBean;
import az.express.edf.persistence.entity.Document;
import az.express.edf.persistence.repository.DocumentRepository;
import az.express.edf.persistence.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private static Logger LOGGER = LogManager.getLogger(DocumentServiceImpl.class);

    private final DocumentRepository documentRepository;

    @Override
    public void update(Long id, String filePath) {
        try {
            Document document = documentRepository.findByDocumentId(id);
            document.setFilePath(filePath);
            documentRepository.save(document);
            LOGGER.info("Save DB Document name!");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("Non Update DB Documnet!");
        }
    }
}
