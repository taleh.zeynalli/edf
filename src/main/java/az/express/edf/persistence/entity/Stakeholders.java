package az.express.edf.persistence.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "STAKEHOLDERS",schema = "EDF")
public class Stakeholders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long stakeholdersId;
    private String tin;
    private String fullName;
    private String contactNumber;
    private String email;
}
