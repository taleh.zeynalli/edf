package az.express.edf.persistence.entity;


import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "APPLICATION", schema = "EDF")
public class Application extends BaseEnt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long applicationId;
    private BigDecimal amount;
    private Integer duration;
    private String purpose;
    private String loanType;
    private String status;
    private BigDecimal interestRate;
    private BigDecimal commission;
    private LocalDate offerDate;
    private BigDecimal offerAmount;
    private Integer offerDuration;
    private Integer offerGracePeriod;
    private String taxesStatus;
    private String bankStatus;
    private String edfStatus;

    @Column(name = "DOCUMENT_REVIEW_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date documentReviewDate;

    @Column(length = 2048)
    private String bankRejectReason;
    @Column(length = 2048)
    private String taxesRejectReason;
    @Column(length = 2048)
    private String edfRejectReason;

    private LocalDate submittedDate;

    private Integer gracePeriod;

    private Boolean hasNextPage;

    private Long lastPageNumber;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Company company;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "APPLICATION_ID")
    private List<Stakeholders> stakeholders;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Document document;

    @PrePersist
    public void persist() {
        setCreatedDate(new Date());
    }

    @PreUpdate
    public void update() {
        setUpdatedDate(new Date());
    }

}
