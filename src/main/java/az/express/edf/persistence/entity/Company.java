package az.express.edf.persistence.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "COMPANY", schema = "EDF")
public class Company {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long companyId;
    private String companyName;
    private String registrationForm;
    private String serialNumber;
    private String pin;
    private LocalDate incorporationDate;
    private String legalAddress;
    private String actualAddress;

}
