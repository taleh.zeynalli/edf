package az.express.edf.exception.error;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponseBean   {
    private String code;
}
