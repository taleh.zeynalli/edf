package az.express.edf.exception.handler;

import az.express.edf.exception.error.ErrorResponseBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger LOGGER = LogManager.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    @ResponseBody
    public final ResponseEntity<ErrorResponseBean> HttpClientErrorExceptionBadRequest(HttpClientErrorException ex, WebRequest request) throws JsonProcessingException {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpClientErrorException.Forbidden.class)
    @ResponseBody
    public final ResponseEntity<ErrorResponseBean> HttpClientErrorExceptionForbiddenRequest(HttpClientErrorException ex, WebRequest request) throws JsonProcessingException {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    @ResponseBody
    public final ResponseEntity<ErrorResponseBean> HttpClientErrorExceptionNotFound(HttpClientErrorException ex, WebRequest request) {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpClientErrorException.Unauthorized.class)
    @ResponseBody
    public final ResponseEntity<ErrorResponseBean> HttpClientErrorExceptionUnauthorized(HttpClientErrorException ex, WebRequest request) throws JsonProcessingException {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
    }


    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        ex.printStackTrace();
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);

    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<ErrorResponseBean> MethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, WebRequest request) {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        LOGGER.error("ERROR - " + ex.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ErrorResponseBean error = new ErrorResponseBean(ex.getLocalizedMessage());
        ex.printStackTrace();
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
