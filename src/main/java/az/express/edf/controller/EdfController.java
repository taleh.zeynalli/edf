package az.express.edf.controller;

import az.express.edf.beans.request.ApproveRequestBean;
import az.express.edf.beans.request.RejectRequestBean;
import az.express.edf.beans.response.ApplicationResponseBean;
import az.express.edf.beans.response.ApplicationsBean;
import az.express.edf.config.ClientConfig;
import az.express.edf.persistence.service.ApplicationService;
import az.express.edf.persistence.service.DocumentService;

import lombok.RequiredArgsConstructor;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.*;


@RestController
@RequiredArgsConstructor
public class EdfController {

    private final static Logger LOGGER = LogManager.getLogger(EdfController.class);

    private final RestTemplate restTemplate;

    private final ClientConfig clientConfig;

    private final ApplicationService applicationService;

    private final DocumentService documentService;

    @Value("${edf.url.prod}")
    private String edfUrl;

    @GetMapping("/applications")
    public ApplicationResponseBean getApplicationList(@RequestParam Integer page, @RequestParam Integer count) {
        LOGGER.info("Get application list!page=" + page + ", count=" + count);
        String url = edfUrl + "applications" + "?page=" + page + "&count=" + count;
        ResponseEntity<ApplicationResponseBean> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.GET, null), ApplicationResponseBean.class
        );
        LOGGER.info("Get Application list Response! application size - " + responseEntity.getBody().getApplications().size()
                + " | has next page - " + responseEntity.getBody().getHasNextPage() + " | last page number - " + responseEntity.getBody().getLastPageNumber());
        for (ApplicationsBean applicationsBean : responseEntity.getBody().getApplications()) {
            applicationService.save(applicationsBean);

        }
        return responseEntity.getBody();
    }

    @GetMapping("/applications/{id}")
    public ApplicationsBean getApplication(@PathVariable("id") Integer id) {
        LOGGER.info("Request  application by Id from EDF! application id - " + id);
        String url = edfUrl + "applications/" + id;
        ResponseEntity<ApplicationsBean> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.GET, null), ApplicationsBean.class);
        LOGGER.info("Application  response info from EDF! response - " + responseEntity.getBody());
        applicationService.save(responseEntity.getBody());
        return responseEntity.getBody();
    }

    @PutMapping("applications/{id}/approve")
    public ApplicationsBean approve(@PathVariable("id") Integer id, @RequestBody ApproveRequestBean approveRequestBean) {
        LOGGER.info("Request approve  application ! Id - " + id + " | Request body - " + approveRequestBean.toString());
        String url = edfUrl + "applications/" + id + "/approve";
        ResponseEntity<ApplicationsBean> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.PUT, approveRequestBean), ApplicationsBean.class);
        LOGGER.info("Approve Response! response - " + responseEntity.getBody());
        applicationService.update(responseEntity.getBody());
        return responseEntity.getBody();
    }

    @PutMapping("applications/{id}/reject")
    public ApplicationsBean reject(@PathVariable("id") Long id, @RequestBody RejectRequestBean rejectRequestBean) {
        LOGGER.info("Request edf reject  application ! Id - " + id + "request body - " + rejectRequestBean.toString());
        String url = edfUrl + "applications/" + id + "/reject";
        ResponseEntity<ApplicationsBean> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.PUT, rejectRequestBean), ApplicationsBean.class);
        LOGGER.info("Reject Response! response - " + responseEntity.getBody());
        applicationService.update(responseEntity.getBody());
        return responseEntity.getBody();
    }

    @PutMapping("applications/{id}/review")
    public ResponseEntity<Void> review(@PathVariable("id") Long id) {
        LOGGER.info("Request edf review  application ! Id - " + id);
        String url = edfUrl + "applications/" + id + "/review";
        ResponseEntity<ApplicationsBean> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.PUT, null), ApplicationsBean.class);
        LOGGER.info("Application Review Response info! response - " + responseEntity.getStatusCode());
        applicationService.update(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/documents/{id}")
    public String documents(@PathVariable("id") Long id) {

        LOGGER.info("Request Documents by Id from EDF! Id - " + id);
        String url = edfUrl + "documents/" + id;
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(clientConfig
                .requestEntity(url, HttpMethod.GET, null), byte[].class);
        LOGGER.info("Response byte array from EDF!");
        byte[] content = responseEntity.getBody();
        String server = "192.168.0.8";
        int port = 21;
        String user = "edfsftp";
        String pass = "$@rgJsn@34!9$";
        String pathFtpServer = "document-" + id + ".adoc";
        FTPClient ftpClient = new FTPClient();
        try {
            LOGGER.info("Connect Ftp server.....");
            ftpClient.connect(server, port);
            LOGGER.info("Connected!");
            LOGGER.info("Login Ftp server.....");
            ftpClient.login(user, pass);
            LOGGER.info("Success Login!");
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.storeFile(pathFtpServer, new ByteArrayInputStream(content));


            LOGGER.info("Save document Ftp server !name - " + pathFtpServer);
            documentService.update(id, pathFtpServer);
            LOGGER.info("*** Process Finished ***");
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return "{\"info\":save document," + "\"name\":" + pathFtpServer + "}";
    }
}



